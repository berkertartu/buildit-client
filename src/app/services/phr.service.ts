import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {PhrDto} from '../models/phr-dto';
import {PhrCommentDto} from '../models/phr-comment-dto';
import {AuthenticationService} from './authentication.service';
import {User} from '../models/User';

@Injectable()
export class PhrService {
  currentUser: User;
  apiUrl = 'http://localhost:8090/api/v1/';
  header = {};

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(
      x => this.currentUser = x);
    this.header = {
      headers: new HttpHeaders()
        .set(
          'Authorization', `Bearer ${this.currentUser.token}`)
    };
  }

  genericRequest(url: string) {
    console.log(this.header);
    return this.http.put(url, null, this.header);
  }

  getPlantSuppliers() {
    return this.http.get(this.apiUrl + 'plant-supplier', this.header);
  }

  getPlantSupplierUrls(id: number) {
    return this.http.get(this.apiUrl + 'plant-supplier/' + id + '/urls', this.header);
  }

  getSupplierCatalogue(url: string) {
    return this.http.get(url);
  }

  getAvailablePlants(url: string, entryId: number, startDate: string, endDate: string) {
    return this.http.get(url + '/?entryId=' + entryId + '&startDate=' + startDate + '&endDate=' + endDate, this.header);
  }

  getConstructionSites() {
    return this.http.get(this.apiUrl + 'construction-site', this.header);
  }

  postPhrRequest(phrDto: PhrDto) {
    return this.http.post(this.apiUrl + 'phr', phrDto, this.header);
  }

  getPendingPhrs() {
    return this.http.get(this.apiUrl + 'phr/pending', this.header);
  }

  getAllPhRS() {
    return this.http.get(this.apiUrl + 'phr', this.header);
  }

  getSupplierPurchaseOrders(url: string) {
    return this.http.get(url);
  }

  getPhrById(id: number) {
    return this.http.get(this.apiUrl + 'phr/' + id, this.header);
  }

  updatePhr(phrDto: PhrDto, id: number) {
    return this.http.put(this.apiUrl + 'phr/' + id, phrDto, this.header);
  }

  getPlantSupplierCustomers() {
    return this.http.get(this.apiUrl + 'plant-supplier/customers', this.header);
  }

  getCustomerOrders(id: number) {
    return this.http.get('http://localhost:8080/api/v1/purchase-order/customer-orders/?customerIdentifier=' + id, this.header);
  }

  getInvoices() {
    return this.http.get(this.apiUrl + 'invoice/', this.header);
  }

  sendRemittanceAdvice(id: number) {
    return this.http.put('http://localhost:8080/api/v1/purchase-order/' + id + '/submit-remittance-advice', null, this.header);
  }

  sendComment(url: string, commentDto: PhrCommentDto) {
    return this.http.post(url, commentDto, this.header);
  }

}

