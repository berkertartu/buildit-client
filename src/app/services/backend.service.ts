export class BackendService {

  private static jwtToken: string;

  static get token(): string {
    return this.jwtToken;
  }

  static set token(value: string) {
    this.jwtToken = value;
  }

  static isLoggedIn(): boolean {
    return !!this.jwtToken;
  }
}
