import {Component, OnInit} from '@angular/core';
import {PhrService} from '../../services/phr.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  invoices: any = [];

  constructor(private phrService: PhrService) {
  }

  ngOnInit() {
    this.getInvoices();
  }

  getInvoices() {
    this.phrService.getInvoices().subscribe(res => {
      this.invoices = res;
    });
  }

  makeRequestToUrl(url: string) {
    this.phrService.genericRequest(url).subscribe(res => {
      console.log(res);
      this.getInvoices();
    }, error => {
      console.log(error);
    });
  }


}
