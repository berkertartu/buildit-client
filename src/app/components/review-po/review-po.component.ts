import {Component, OnInit} from '@angular/core';
import {PhrService} from '../../services/phr.service';

@Component({
  selector: 'app-review-po',
  templateUrl: './review-po.component.html',
  styleUrls: ['./review-po.component.css']
})
export class ReviewPoComponent implements OnInit {

  selectedCustomer: any;
  customers: any = [];
  orders: any = [];
  error: any = null;

  constructor(private phrService: PhrService) {
  }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.phrService.getPlantSupplierCustomers().subscribe(res => {
      this.customers = res;
      console.log(res);
    }, error => {
    });
  }

  getCustomerOrders(id: number) {
    this.phrService.getCustomerOrders(id).subscribe(res => {
      this.orders = res;
    }, error => {
      this.error = error;
    });
  }
}
