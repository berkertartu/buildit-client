import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPhrSiteComponent } from './review-phr-site.component';

describe('ReviewPhrSiteComponent', () => {
  let component: ReviewPhrSiteComponent;
  let fixture: ComponentFixture<ReviewPhrSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewPhrSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPhrSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
