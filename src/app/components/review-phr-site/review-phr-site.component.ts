import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {PhrService} from '../../services/phr.service';
import {PhrDto} from '../../models/phr-dto';
import * as moment from 'moment';
import {PhrCommentDto} from '../../models/phr-comment-dto';

@Component({
  selector: 'app-review-phr-site',
  templateUrl: './review-phr-site.component.html',
  styleUrls: ['./review-phr-site.component.css']
})
export class ReviewPhrSiteComponent implements OnInit {

  modalRef: BsModalRef;
  pendingPhrs: any = [];
  phrDto = new PhrDto();
  minDate: Date;
  maxDate: Date;
  dateRange: Date[];
  startDate: any;
  constructionSites: any = [];
  selectedConstructionSite: any;
  supplierUrls: any;
  comment: string;
  selectedPhr: any;
  phrCommentDto = new PhrCommentDto();
  minExtDate: Date;
  extDate: Date;

  constructor(private modalService: BsModalService, private phrService: PhrService) {

  }

  ngOnInit() {
    this.getPendingPhrs();
    this.getConstructionSites();
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate());
  }

  makeRequestToUrl(url: string) {
    this.phrService.genericRequest(url).subscribe(res => {
      console.log(res);
      this.getPendingPhrs();
    }, error => {
      console.log(error);
    });
  }

  getConstructionSites() {
    this.phrService.getConstructionSites().subscribe(res => {
      this.constructionSites = res;
      console.log(this.constructionSites);
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-md modal-dialog-centered',
    });
  }

  getPendingPhrs() {
    this.phrService.getAllPhRS().subscribe(res => {
      this.pendingPhrs = res;
      console.log(this.pendingPhrs);
    });
  }

  getPhrById(id: number) {
    this.phrService.getPhrById(id).subscribe(res => {
        // @ts-ignore
        this.phrDto = res;
        this.minDate = new Date();
        this.maxDate = new Date();
        this.minDate.setDate(new Date(this.phrDto.rentalPeriod.startDate).getDate());
        this.maxDate.setDate(new Date(this.phrDto.rentalPeriod.endDate).getDate());
        this.minExtDate = new Date();
        this.minExtDate.setDate(new Date(this.phrDto.rentalPeriod.endDate).getDate() + 1);
        this.getPlantSupplierUrl();
        console.log(this.phrDto);
      },
      error => {

      });
  }

  close(): void {
    this.modalRef.hide();
  }

  getPlantSupplierUrl() {
    this.phrService.getPlantSupplierUrls(this.phrDto.plantSupplierId).subscribe(res => {
      this.supplierUrls = res;
    });
  }

  modifyPhr() {
    this.phrDto.rentalPeriod.startDate = moment(this.dateRange[0]).format('YYYY-MM-DD');
    this.phrDto.rentalPeriod.endDate = moment(this.dateRange[1]).format('YYYY-MM-DD');
    this.phrDto.constructionSiteId = this.selectedConstructionSite.id;
    this.phrDto.constructionSiteName = this.selectedConstructionSite.name;
    this.phrService.updatePhr(this.phrDto, this.phrDto._id).subscribe(res => {
      console.log(res);

      this.getPendingPhrs();
    });
  }

  extendPhr() {
    this.phrDto.rentalPeriod.endDate = moment(this.extDate).format('YYYY-MM-DD');
    console.log(this.phrDto);
    this.phrService.updatePhr(this.phrDto, this.phrDto._id).subscribe(res => {
      console.log(res);
      this.getPendingPhrs();
    });
  }
}
