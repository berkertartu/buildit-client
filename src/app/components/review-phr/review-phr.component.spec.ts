import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPhrComponent } from './review-phr.component';

describe('ReviewPhrComponent', () => {
  let component: ReviewPhrComponent;
  let fixture: ComponentFixture<ReviewPhrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewPhrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPhrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
