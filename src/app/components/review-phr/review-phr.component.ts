import {Component, OnInit, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {PhrService} from '../../services/phr.service';
import {PhrDto} from '../../models/phr-dto';
import * as moment from 'moment';
import {PhrCommentDto} from '../../models/phr-comment-dto';

@Component({
  selector: 'app-review-phr',
  templateUrl: './review-phr.component.html',
  styleUrls: ['./review-phr.component.css']
})
export class ReviewPhrComponent implements OnInit {

  modalRef: BsModalRef;
  pendingPhrs: any = [];
  phrDto = new PhrDto();
  constructionSites: any = [];
  selectedConstructionSite: any;
  minDate: Date;
  maxDate: Date;
  dateRange: Date[];
  pendingPlantsError = false;
  phrCommentDto = new PhrCommentDto();
  comment: string;
  selectedPhr: any;

  constructor(private modalService: BsModalService, private phrService: PhrService) {
  }

  ngOnInit() {
    this.getPendingPhrs();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-md modal-dialog-centered',
    });
  }

  openModalWithPhr(phr: PhrDto, template: TemplateRef<any>, url: string, id: number) {
    this.minDate = new Date();
    this.maxDate = new Date();
    this.minDate.setDate(new Date(phr.rentalPeriod.startDate).getDate());
    this.maxDate.setDate(new Date(phr.rentalPeriod.endDate).getDate());
    this.openModal(template);
    this.selectedPhr = phr;
    this.getConstructionSites();

  }

  close(): void {
    this.modalRef.hide();
  }

  getPendingPhrs() {
    this.phrService.getPendingPhrs().subscribe(res => {
        this.pendingPhrs = res;
        console.log(this.pendingPhrs);
      },
      error => {
        this.pendingPlantsError = true;
      });
  }

  makeRequestToUrl(url: string) {
    this.phrService.genericRequest(url).subscribe(res => {
      console.log(res);
      this.getPendingPhrs();
    }, error => {
      console.log(error);
    });
  }

  modifyPhr() {
    console.log(this.phrDto);
    this.phrCommentDto.comment = this.comment;
    this.phrCommentDto.worksEngineerId = 1;
    this.phrCommentDto.worksEngineerFirstName = 'Tarlan';
    this.phrCommentDto.worksEngineerLastName = 'Hasanli';
    this.phrCommentDto.worksEngineerUserName = 'tarlan.h';
    this.phrDto.rentalPeriod.startDate = moment(this.dateRange[0]).format('YYYY-MM-DD');
    this.phrDto.rentalPeriod.endDate = moment(this.dateRange[1]).format('YYYY-MM-DD');
    this.phrDto.constructionSiteId = this.selectedConstructionSite.id;
    this.phrDto.constructionSiteName = this.selectedConstructionSite.name;
    this.phrService.updatePhr(this.phrDto, this.selectedPhr._id).subscribe(res => {
      console.log(res);
      this.getPendingPhrs();
    });
    let commentHref: string;
    let rejectHref: string;
    for (const link of this.selectedPhr.links) {
      if (link.rel === 'comment') {
        commentHref = link.href;
      }
      if (link.rel === 'reject') {
        rejectHref = link.href;
      }
    }

    this.phrService.sendComment(commentHref, this.phrCommentDto).subscribe(res => {
      console.log(res);
    });
  }

  getConstructionSites() {
    this.phrService.getConstructionSites().subscribe(res => {
      this.constructionSites = res;
    });
  }

  rejectPHR() {

    this.phrCommentDto.phrId = this.selectedPhr.id;
    this.phrCommentDto.comment = this.comment;
    this.phrCommentDto.worksEngineerId = 1;
    this.phrCommentDto.worksEngineerFirstName = 'Tarlan';
    this.phrCommentDto.worksEngineerLastName = 'Hasanli';
    this.phrCommentDto.worksEngineerUserName = 'tarlan.h';

    let commentHref: string;
    let rejectHref: string;
    for (const link of this.selectedPhr.links) {
      if (link.rel === 'comment') {
        commentHref = link.href;
      }
      if (link.rel === 'reject') {
        rejectHref = link.href;
      }
    }

    this.phrService.sendComment(commentHref, this.phrCommentDto).subscribe(res => {
      console.log(res);
    });
    this.makeRequestToUrl(rejectHref);
  }

}
