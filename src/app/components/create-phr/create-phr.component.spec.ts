import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePhrComponent } from './create-phr.component';

describe('CreatePhrComponent', () => {
  let component: CreatePhrComponent;
  let fixture: ComponentFixture<CreatePhrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePhrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePhrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
