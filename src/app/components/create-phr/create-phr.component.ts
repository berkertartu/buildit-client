import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {PhrService} from '../../services/phr.service';
import * as moment from 'moment';
import {PhrDto} from '../../models/phr-dto';
import {Router} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {AuthenticationService} from '../../services/authentication.service';
import {User} from '../../models/User';

@Component({
  selector: 'app-create-phr',
  templateUrl: './create-phr.component.html',
  styleUrls: ['./create-phr.component.css']
})
export class CreatePhrComponent implements OnInit {

  modalRef: BsModalRef;
  @ViewChild('template') public modal: TemplateRef<any>;
  currentUser: User;

  suppliers: any = [];
  supplierUrls: any;
  supplierCatalogue: any = [];
  selectedSupplier: any;

  selectedPlant: any;
  availablePlant: any = null;
  availablePlantError: any = null;

  // VARIABLES FOR TAB
  isInspectPlantDisabled = true;
  isMakeRequestDisabled = true;
  isFindPlantActive = true;
  isInspectActive = false;
  isMakeRequestActive = false;

  startDate: string;
  endDate: string;
  minDate: Date;
  dateRange: Date[];

  constructionSites: any = [];
  selectedConstructionSite: any;
  phrDto = new PhrDto();
  phrSuccess: boolean;

  constructor(private phrService: PhrService, private router: Router, private modalService: BsModalService,
              private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(
      x => this.currentUser = x);
    console.log(this.currentUser);
  }

  ngOnInit() {
    this.getConstructionSites();
    this.phrService.getPlantSuppliers().subscribe(res => {
      this.suppliers = res;
    });
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate());
  }

  getSupplierCatalog(id: number) {
    this.phrService.getPlantSupplierUrls(id).subscribe(res => {
      this.supplierUrls = res;
      console.log(this.supplierUrls);
      this.phrService.getSupplierCatalogue(this.supplierUrls.catalogueUrl).subscribe(catalogue => {
        this.supplierCatalogue = catalogue;
        console.log(catalogue);
      });
    });
  }

  selectPlant(id: number) {
    this.selectedPlant = this.supplierCatalogue[id];
    this.isInspectActive = true;
    this.isFindPlantActive = false;
    this.isInspectPlantDisabled = false;
  }

  findAvailablePlants() {

    this.startDate = moment(this.dateRange[0]).format('YYYY-MM-DD');
    this.endDate = moment(this.dateRange[1]).format('YYYY-MM-DD');

    console.log(moment(this.startDate).toISOString() + '' + moment(this.endDate).toLocaleString());

    this.phrService.getAvailablePlants(this.supplierUrls.availableUrl, this.selectedPlant._id, this.startDate, this.endDate)
      .subscribe(res => {
          this.availablePlant = res;
          console.log(this.availablePlant);
        },
        error => {
          this.availablePlantError = error;
        });
  }

  deleteSelectedItem() {
    this.selectedPlant = null;
    this.isInspectActive = false;
    this.isFindPlantActive = true;
    this.isInspectPlantDisabled = true;
  }

  selectAvailableItem() {
    this.isInspectActive = false;
    this.isMakeRequestActive = true;
    this.isMakeRequestDisabled = false;
  }

  getConstructionSites() {
    this.phrService.getConstructionSites().subscribe(res => {
      this.constructionSites = res;
      console.log(this.constructionSites);
    });
  }

  createPhrRequestBody() {
    this.phrDto.totalPrice = this.availablePlant.totalPrice;
    this.phrDto.plantInventoryItemId = this.availablePlant._id;
    this.phrDto.rentalPeriod.startDate = this.startDate;
    this.phrDto.rentalPeriod.endDate = this.endDate;
    this.phrDto.constructionSiteId = this.selectedConstructionSite.id;
    this.phrDto.plantSupplierId = this.selectedSupplier.id;
    this.phrDto.siteEngineerId = 1;

    console.log(this.phrDto);
    this.phrService.postPhrRequest(this.phrDto).subscribe(res => {
      if (res) {
        this.phrSuccess = true;
        this.openModal(this.modal);
      } else {
        this.phrSuccess = false;
        this.openModal(this.modal);
      }
    });
  }

  redirectTo(uri) {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate([uri]));
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      class: 'modal-md modal-dialog-centered',
      ignoreBackdropClick: true
    });
  }

  confirm(): void {
    this.modalRef.hide();
  }

}
