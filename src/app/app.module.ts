import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {FooterComponent} from './components/footer/footer.component';
import {CreatePhrComponent} from './components/create-phr/create-phr.component';
import {InvoiceComponent} from './components/invoice/invoice.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';

import {TabsModule} from 'ngx-bootstrap/tabs';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {ReviewPhrComponent} from './components/review-phr/review-phr.component';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ModalModule} from 'ngx-bootstrap/modal';


import {PhrService} from './services/phr.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReviewPoComponent} from './components/review-po/review-po.component';
import {ReviewPhrSiteComponent} from './components/review-phr-site/review-phr-site.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    CreatePhrComponent,
    ReviewPhrComponent,
    InvoiceComponent,
    LoginComponent,
    RegisterComponent,
    ReviewPoComponent,
    ReviewPhrSiteComponent,

  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    PhrService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
