import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreatePhrComponent} from './components/create-phr/create-phr.component';
import {ReviewPhrComponent} from './components/review-phr/review-phr.component';
import {InvoiceComponent} from './components/invoice/invoice.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {ReviewPhrSiteComponent} from './components/review-phr-site/review-phr-site.component';
import {ReviewPoComponent} from './components/review-po/review-po.component';
import {AuthGuard} from './guards/auth.guard';
import {SiteGuard} from './guards/role.guard';
import {WorksGuard} from './guards/works.guard';


const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'phr', component: CreatePhrComponent, canActivate: [AuthGuard]},
  {path: 'review-phr', component: ReviewPhrComponent, canActivate: [AuthGuard]},
  {path: 'invoices', component: InvoiceComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'review-phr-site', component: ReviewPhrSiteComponent, canActivate: [AuthGuard]},
  {path: 'review-po', component: ReviewPoComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
