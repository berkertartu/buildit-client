import {RentalPeriodDto} from './rental-period-dto';

export class PhrDto {
  _id: number;
  constructionSiteId: number;
  constructionSiteName: string;
  plantSupplierId: number;
  plantInventoryItemId: number;
  rentalPeriod: RentalPeriodDto = new RentalPeriodDto();
  totalPrice: number;
  siteEngineerId: number;
}
