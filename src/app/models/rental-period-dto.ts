export class RentalPeriodDto {
  startDate: string;
  endDate: string;
}
