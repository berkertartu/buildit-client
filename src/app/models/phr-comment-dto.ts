export class PhrCommentDto {
  id: number;
  comment: string;
  phrId: number;
  worksEngineerId: number;
  worksEngineerFirstName: string;
  worksEngineerLastName: string;
  worksEngineerUserName: string;
}
